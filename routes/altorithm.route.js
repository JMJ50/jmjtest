const router = require('express').Router();


router.post('/test001', (req, res) => {

    let samples = req.body;

    const solution = (str) => {
        var answer;

        var value = Number(str);

        if(value){
            str.length === 4 || str.length === 6 ? answer = true : answer = false;
        }else{
            answer = false;
        };

        return answer;
    };

    const result = samples.map( (testcase) => {
        testcase.output = solution(testcase.input);
        return testcase;
    });

    res.send(result);
});

router.post('/test002', (req, res) => {

    let samples = req.body;

    const solution = (str) => {
        var answer = [];
        str = String(str);
        for(var idx = str.length - 1; idx >= 0; idx--){
            var value = parseInt(str[idx]);
            answer.push(value);
        };

        return answer;
    };

    const result = samples.map( (testcase) => {
        testcase.output = solution(testcase.input);
        return testcase;
    });

    res.send(result);
});

router.post('/test003', (req, res) => {

    let samples = req.body;

    const solution = (m, tree) => {
        var answer = "";
    
        var low = 0;
        var high = Math.max(...tree);
        
        while(low < high){
            var std = Math.floor((low + high) / 2); //기준점
            var sum = 0;

            for(var idx = 0; idx < tree.length; idx ++){
                var temp = tree[idx] - std;
                if(temp > 0)sum += temp;
            };

            if(sum > m){
                low = std + 1;
            }else if(sum < m){
                high = std;
            }else{
                break;
            };
        };

        answer = std;

        return answer;
    };

    const result = samples.map( (testcase) => {
        testcase.output = solution(testcase.m, testcase.tree.sort());
        return testcase;
    });

    res.send(result);
});

router.post('/test004', (req, res) => {

    let samples = req.body;

    const solution = (n, budget, m) => {
        var input = budget.sort(function (a, b) { return a - b });
        var answer = 0;
        var low = budget[0];
        var high = input[input.length - 1];

        while (low <= high) {
            var sum = 0;
            var oldStd = std;
            var std = Math.floor((low + high) / 2); //기준점
            if (oldStd === std) break;


            for (var idx = 0; idx < input.length; idx++) {
                input[idx] <= std ? sum += input[idx] : sum += std;
            };

            if (sum > m) {
                high = std;
            } else if (sum <= m) {
                low = std;
            };


        };
        answer = std;


        return answer;
    };

    const result = samples.map( (testcase) => {
        testcase.output = solution(testcase.n, testcase.budget, testcase.m);
        return testcase;
    });

    res.send(result);
});

router.post('/test005', (req, res) => {
    
    let samples = req.body;

    const solution = (n, times) => {
        var answer = 0;
        var cnt = 0;
        var low = 7;
        var high = 10;

        while (low <= high) {
            // var oldStd = std;
            var std = Math.floor((low * high) / 2); //기준점
            // if (oldStd === std) break;
            

            if(cnt > std){

            }else if(cnt < std){

            }else{
                break;
            };
        };

            
            return answer;
    };

    const result = samples.map( (testcase) => {
        testcase.output = solution(testcase.n, testcase.times);
        return testcase;
    });

    res.send(result);
});

module.exports = router;